---
title: Aliases and Bash Customization
teaching: 10 minutes
exercises: 0
questions:
- How do I customize my bash environment?
objectives:
- Create aliases.
- Add customizations to the `.bashrc` and `.bash_profile` files.
- Change the prompt in a bash environment.
keypoints:
- Aliases are used to create shortcuts or abbreviations
- The `.bashrc` and `.bash_profile` files allow us to customize our 
  bash environment.
- The `PS1` system variable can be changed to customize your bash
  prompt.

---

Bash allows us to customize our environments to fill our own
particular needs.

## Aliases

Sometimes we need to use long commands that have to be typed over and
over again.  Fortunately, the `alias` command allows us to create
shortcuts for these long commands.

As an example, let's create aliases for going up one, two, or three
directories.

~~~
$ alias up='cd ..'
$ alias upup='cd ../..'
$ alias upupup='cd ../../..'
~~~
{: .bash}

Let's try these commands out.

~~~
$ cd /usr/local/bin
$ upup
$ pwd
~~~
{: .bash}

~~~
/usr
~~~
{: .output}

In a previous episode, we learned that some file operations (`cp`, `mv`, and `rm`),
by default, operate on existing files without warning.
For many users, the following aliases are useful to save them from accidental
mistakes:

~~~
$ alias cp='cp -i'
$ alias mv='mv -i'
$ alias rm='rm -i'
~~~
{: .bash}

Here is another example to use `zback` (a Windows backup program) to
backup your home directory in a single command call.
to run `/bin/zback` with a specific set of arguments:

~~~
alias backup=/bin/zback -v --nostir -R 20000 $HOME $BACKUP_DIR
~~~
{: .bash}

We can also remove a shortcut with `unalias`:

~~~
$ unalias upupup
~~~
{: .bash}

To see what aliases are available, simply type `alias` with no argument.
On Turing, this may lead to the following output:
~~~
$ alias
~~~
{: .bash}
~~~
alias l.='ls -d .* --color=auto'
alias ll='ls -l --color=auto'
alias ls='ls --color=auto'
alias mc='. /usr/libexec/mc/mc-wrapper.sh'
alias vi='vim'
alias which='alias | /usr/bin/which --tty-only --read-alias --show-dot --show-tilde'
~~~
{: .output}

> ## How does an alias work?
> When the shell encounter the first word that matches an alias, it is first
> substituted with the definition of that alias, then executed as usual.
> So, when we type `ll` <kbd>Enter</kbd> on our shell, the command line will be
> substituted with `ls -l --color=auto` before executed.
> Arguments that follow the alias will simply be put on the right hand of the
> alias definition.
> For example, `ll *.pdb *.txt` will become `ls -l --color=auto *.pdb *.txt`
> before further expansions takes place.
>
> Some astute reader may notice that `ls` is aliased to `ls --color=auto`.
> Doesn't this lead to an infinite expansion? No.
> Alias substitution is only performed once.
{: .callout}

If we create one of these aliases in a bash session, they will only
last until the end of that session. Fortunately, bash allows us to
specify customizations that will work whenever we begin a new bash
session.

## Bash customization files

Bash environments can be customized by adding commands to the
`.bashrc`, `.bash_profile`, and `.bash_logout` files in our home
directory.  The `.bashrc` file is executed whenever entering
interactive non-login shells whereas `.bash_profile` is executed for
login shells.  If the `.bash_logout` file exists, then it will be run
after exiting a shell session.

> ## Customization on Turing
>
> On Turing HPC, the customization should be put inside
> `~/.turing_bashrc`, `~/.turing_bash_profile`, and `~/.turing_bash_logout`
> because ODU HPC has grown!
> There is another cluster called Wahab, so each cluster may need its own
> customization.
{: .callout}

Let's add the above commands to our `.bashrc` file.

~~~
$ echo "alias up='cd ..'" >> ~/.bashrc
$ tail -n 1 ~/.bashrc
~~~
{: .bash}

~~~
alias up='cd ..'
~~~
{: .output}

Alternatively, one can use text editor to edit `~/.bashrc` or `~/.turing_bashrc`.

We can execute the commands in `.bashrc` using `source`:

~~~
$ source ~/.bashrc
$ cd /usr/local/bin
$ up
$ pwd
~~~
{: .bash}

~~~
/usr/local
~~~
{: .output}

Having to add customizations to two files can be cumbersome.  It we
would like to always use the customizations in our `.bashrc` file,
then we can add the following lines to our `.bash_profile` file.

~~~
if [ -f $HOME/.bashrc ]; then
        source $HOME/.bashrc
fi
~~~
{: .bash}

## Customizing your prompt

We can also customize our bash prompt by setting the `PS1` system
variable. To set our prompt to be `$ `, then we can run the command

~~~
export PS1='$ '
~~~
{: .bash}

To set the prompt to `$ ` for all bash sessions, add this line to the
end of `.bashrc`.

Further [bash prompt
customizations](https://www.howtogeek.com/307701/how-to-customize-and-colorize-your-bash-prompt)
are possible.  To have our prompt be `username@hostname[directory]: `,
we would set

~~~
export PS1='\u@\h[\W]: '
~~~
{: .bash}

where `\u` represents username, `\h` represents hostname, and `\W`
represents the current directory.
Note that it is recommended to use single quote for a prompt, unless a variable
expansion is intended.
The reason is that the prompt string frequently needs to contain dollar (`$`)
and backslash (`\`) characters, which have special meanings in double-quoted
strings.

`PS1` is an example of a shell variable.
In the next episode, we will cover shell variables in more detail.