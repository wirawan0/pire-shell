---
title: "Cheatsheets for Frequently Used UNIX and HPC Commands"
---

## UNIX Shell

> 1. Querying the shell's current working directory:
>
>        pwd
>
> 2. List content of current directory
>
>        ls 
>
> 3. Change directory:
>
>        cd DIR
>
> 4. Copy file:
>
>        cp [-p] SRC_FILE DEST_FILE
>        cp [-p] SRC_FILE [SRC_FILE2 ...] DEST_DIR
>
>    Optional `-p` flag preserves file modification time and permissions.
>    The first syntax can be used to copy the file and create a new
>    filename.
>
> 5. Copy directory tree:
>
>        cp -r [-p] SRC_DIR DEST_DIR
>
>    If `DEST_DIR` exist, then a subdirectory with the base name of
>    `SRC_DIR` will be made to contain the copy; otherwise, the copy will
>    be stored in `DEST_DIR`.
>
> 6. Rename a file or directory:
>
>        mv SRC_FILE DEST_FILE
>        mv SRC_FILE [SRC_FILE2 ...] DEST_DIR
>        mv SRC_DIR DEST_DIR
>
> 7. Inspecting the content of a (text) file:
>
>        cat FILE
>        more FILE
>        less FILE
>
>    Use `cat` for relatively short text file.
>    `more` command allows pause between pages, but no scrolling backward.
>    `less` is the most sophisticated of all, allowing forward and backward scrolls,
>    and more.
>
> 8. Determining the type of a file:
>
>        file FILE
>
> 9. Text editors for terminal:
>
>        nano [FILE]
>        vi [FILE]
>        emacs [-nw] [FILE]
>
>    New users are encouraged to use `nano`, which is the easiest to use of all.
>    `vi` is the classic editor on UNIX platforms
>    (it is actually `vim` on many modern Linux distributions).
>    `emacs` is another favorite editor on UNIX-like platforms.
>    The `-nw` optional flag can be used to suppress the X11 GUI window of Emacs, if
>    your SSH connection supports X11 programs.
>
> 10. Printing beginning or end of a text file:
>
>         head -n NUMLINES FILE
>         tail -n NUMLINES FILE
>
> 11. Sort lines of a text file:
>
>         sort [OPTIONS] FILE
>
>     a. numerical (first word on the line)
>
>            sort -n FILE
>
>     b. reverse order
>
>            sort -r FILE

{% include links.md %}
